# Detekce činností na letištní stojánce
## Popis
Použití modelu YOLOv8 pro detekci činností na letištní stojánce.

Detekce třech klíčových činností:
1. Otevřené dveře zavazadlového prostoru
2. Parkování letadla
3. Připojení nástupního mostu k letadlu

### Detector
Umožňuje provádět analýzu na videích ve složce raw_videos.

### Livestream
Umožňuje provádět analýzu na livestreamu z letiště Tokachi-Obihiro.

### MetricCalculator
Provádí výpočet metrik, porovnává intervaly 
uložené v souboru v outputs/intervals s others/timestamps.csv

## Ukázky
### Ukázka detekce ze streamu 15.5.2024 ve 12:30 - 13:00
Letadlo je nakládáno a je připojen tunel
![stream](readme_images/stream.png)
Proces odbavování skončil, ale letadlo stále stojí na parkovacím místě
![stream_detached](readme_images/stream_detached.png)
Letadlo odjíždí ze stojánky
![stream_leaving](readme_images/stream_leaving.png)

## Instalace
Ve složce environment je soubor environment.yml, 
který obsahuje informace o virtuálním prostředí a jeho balíčkách, 
které jsou nutné ke spouštění jupyter notebooků

conda env create -f environment/environment.yml