import copy
import json
import os

import cv2
import numpy as np
import torch
from tqdm import tqdm


def close_to_point(input_point, reference_point, max_distance):
    """
    Checks if the input point is within a certain distance from the reference point.

    :param input_point: The point to check.
    :param reference_point: The reference point.
    :param max_distance: The maximum allowable distance.
    :return: True if the input point is within the max distance from the reference point, otherwise False.
    """
    return np.linalg.norm(input_point - reference_point) < max_distance


class VideoAnalyzer:
    def __init__(self, confidence_thresholds: list, parking_position, confirmation_delays: dict):
        """
        Initializes the VideoAnalyzer with the given parameters.

        :param confidence_thresholds: List of confidence thresholds for each class.
        :param parking_position: The position where the wheel needs to be to indicate the airplane is parked.
        :param confirmation_delays: Delays for confirming different statuses.
        """
        # parking position where wheel needs to be to say airplane is parked
        self.parking_position = parking_position
        # Confidence threshold for each class
        self.confidence_thresholds = confidence_thresholds
        self.video_information = {}
        self.confirmation_delays = confirmation_delays

    @staticmethod
    def __analyze_condition(condition_met, status, status_key,
                            status_counter, step_time, seconds_delay, frame_index, statuses):
        """
        Analyzes if a condition is met and updates the status accordingly.

        :param condition_met: Boolean indicating if the condition is met.
        :param status: Dictionary containing current statuses.
        :param status_key: Key for the status being analyzed.
        :param status_counter: Counter for the status.
        :param step_time: Time step between frames.
        :param seconds_delay: Delay in seconds to confirm the status.
        :param frame_index: Index of the current frame.
        :param statuses: Dictionary storing statuses over time.
        :return: Updated status and status_counter.
        """
        # Check if the condition was met
        if condition_met:
            # The condition was met, increase the counter
            status_counter[status_key][0] += step_time
            if status_counter[status_key][0] >= seconds_delay:

                if not status[status_key]:
                    # Change condition to true
                    status[status_key] = True
                    statuses[status_key].append([step_time * frame_index])
            status_counter[status_key][1] = 0
        else:
            # Condition was not met
            status_counter[status_key][1] += step_time
            if status[status_key] and status_counter[status_key][1] >= seconds_delay:
                # Condition was met before but now its not
                status[status_key] = False
                statuses[status_key][-1].append(step_time * frame_index)
            status_counter[status_key][0] = 0  # Reset counter since wheel is not in parking spot

        return status, status_counter

    def __analyze_frame(self, boxes, status, status_counter, step_time, frame_index, statuses):
        """
        Analyzes a single frame for different conditions and updates the statuses.

        :param boxes: Detected boxes in the frame.
        :param status: Dictionary containing current statuses.
        :param status_counter: Counter for the statuses.
        :param step_time: Time step between frames.
        :param frame_index: Index of the current frame.
        :param statuses: Dictionary storing statuses over time.
        :return: Updated status and status_counter.
        """
        # Open Doors
        door_open = torch.any(boxes.cls == 0)
        status, status_counter = VideoAnalyzer.__analyze_condition(
            door_open, status, "Door(s) open", status_counter, step_time,
            seconds_delay=self.confirmation_delays["Door(s) open"], frame_index=frame_index,
            statuses=statuses)

        # Tunnel attachment
        tunnel_attached = torch.any(boxes.cls == 1)
        status, status_counter = VideoAnalyzer.__analyze_condition(tunnel_attached, status,
                                                                   "Tunnel attached", status_counter,
                                                                   step_time,
                                                                   seconds_delay=self.confirmation_delays[
                                                                       "Tunnel attached"],
                                                                   frame_index=frame_index,
                                                                   statuses=statuses)

        # Wheel positions
        wheel_indices = (boxes.cls == 2).nonzero(as_tuple=False).flatten()
        if wheel_indices.numel() != 0:
            # take just the highest confidence wheel detection
            wheel_xywhn = boxes.xywhn[wheel_indices[0]].flatten().cpu().numpy()
            wheel_position = wheel_xywhn[:2]
            wheel_width = wheel_xywhn[2]

            # Parking
            parking = close_to_point(wheel_position, self.parking_position, wheel_width * 1.5)
            status, status_counter = VideoAnalyzer.__analyze_condition(parking, status, "Parked", status_counter,
                                                                       step_time,
                                                                       seconds_delay=self.confirmation_delays["Parked"],
                                                                       frame_index=frame_index, statuses=statuses)
        return status, status_counter

    @staticmethod
    def __filter_by_confidence(detection_result, confidence_thresholds: list):
        """
        Filters detection results to keep only those with confidence above the threshold.

        :param detection_result: The result of a detection.
        :param confidence_thresholds: List of minimal confidences for each class.
        :return: Filtered detection results.
        """
        confidences = detection_result.boxes.conf
        class_indices = detection_result.boxes.cls.long()  # Ensure indices are in long format not float
        # Create a tensor of thresholds based on class indices
        thresholds = torch.tensor([confidence_thresholds[i] for i in class_indices], device=confidences.device)
        # Create a mask where confidence is greater than threshold
        mask = confidences > thresholds
        # Filter boxes using the mask
        filtered_detections = copy.deepcopy(detection_result)
        filtered_detections.boxes = detection_result.boxes[mask]
        return filtered_detections

    def analyze_video(self, results, step_time, show_video: bool, save_video: bool, video_name, output_fps=30):
        """
        Analyzes a video to track different statuses. Can show and save the video.

        :param results: List of detection results for each frame.
        :param step_time: Time step between frames.
        :param show_video: Boolean indicating whether to show the video.
        :param save_video: Boolean indicating whether to save the video.
        :param video_name: Name of the video.
        :param output_fps: Frames per second for the output video.
        :return: Dictionary containing video information.
        """
        self.video_information[video_name] = {}
        # dictionary storing important statuses
        status = {"Door(s) open": False, 'Parked': False, "Tunnel attached": False}
        # list of the status dictionaries for each frame
        statuses = {"Door(s) open": [], 'Parked': [], "Tunnel attached": []}
        self.video_information[video_name]["statuses"] = statuses
        # First value counts up seconds when the condition was met,
        # the other value is for when the condition was not met
        status_counter = {"Door(s) open": [0, 0], 'Parked': [0, 0], "Tunnel attached": [0, 0]}
        video_length = 0

        # for i, result in tqdm(enumerate(results), total=len(results)):
        for i, result in enumerate(results):
            if i == 0 and save_video:  # in the first step initializa video_writer
                video_writer = cv2.VideoWriter(f'outputs/videos/{video_name}_fps-{output_fps}.mp4',
                                               cv2.VideoWriter_fourcc(*'H264'), fps=output_fps,
                                               frameSize=result.orig_shape[::-1])
            if result.boxes.cls.size() != 0:  # Filter only if there are detected boxes
                result = VideoAnalyzer.__filter_by_confidence(result, self.confidence_thresholds)
            image_after_deteciton = result.plot()
            status, status_counter = self.__analyze_frame(result.boxes, status, status_counter,
                                                          step_time, frame_index=i, statuses=statuses)
            postprocessed_image = self.__postprocess_image(image_after_deteciton, status)
            video_length += 1
            if save_video:
                video_writer.write(postprocessed_image)
            if show_video:
                # Show the image with detection and status
                cv2.imshow("Detection", postprocessed_image)
                # Wait for a key press to move to the next frame or quit
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
        # cleanup
        if save_video:
            video_writer.release()
        if show_video:
            cv2.destroyAllWindows()

        self.video_information[video_name]["length"] = video_length

        return self.video_information[video_name]

    @staticmethod
    def __postprocess_image(image, status):
        """
        Draws status information on the top left corner of the image.

        :param image: Input image on which to draw the text.
        :param status: Dictionary containing status labels and their boolean values.
        :return: Image with the status information drawn on it.
        """
        font = cv2.FONT_HERSHEY_SIMPLEX
        font_scale = 1.5
        thickness = 2
        line_type = cv2.LINE_AA

        # Starting Y position (a bit down from the top to be visible)

        # Iterate over each status and draw it
        # status format -- status = {"Door(s) open": 0,'Parked': 0, "Waiting for takeoff": 0, "Tunnel attached": 0}
        for i, (key, value) in enumerate(status.items()):
            text = f'{key}: {"Yes" if value else "No"}'
            color = (0, 255, 0) if value else (0, 0, 255)  # Green if true, blue if false
            # Calculate the position to start the text from
            textsize = cv2.getTextSize(text, font, font_scale, thickness)[0]
            text_x = 0  # left side
            line_width = textsize[1] * 1.5
            text_y = int(line_width * (1 + i))
            # Put the text on the image
            cv2.putText(image, text, (text_x, text_y), font, font_scale, color, thickness, line_type)

        return image

    def video_information_to_json(self, file_path: str, rewrite=False):
        """
        Saves the video information to a JSON file.

        :param file_path: Path to the JSON file.
        :param rewrite: Boolean indicating whether to rewrite the file if it exists.
        """
        # Try to load existing data if the file exists
        if os.path.exists(file_path) and not rewrite:
            with open(file_path, 'r') as file:
                data = json.load(file)
            # Merge the current video statuses into the loaded data
            for video, statuses in self.video_information.items():
                if video in data:
                    data[video].update(statuses)  # Merge statuses for existing video entries
                else:
                    data[video] = statuses  # Add new video entry
        else:
            data = self.video_information  # Set to current statuses if rewriting or file doesn't exist

        # Write the merged/updated/new data back to the file
        with open(file_path, 'w') as write_file:
            json.dump(data, write_file, indent=4)
