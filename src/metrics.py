import json
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd


class Metrics:
    def __init__(self, timestamps_path) -> None:
        """
        Initialize Metrics object.

        :param timestamps_path: Path to the timestamps file.
        """
        self.timestamps, self.action_bounds = Metrics.__read_timestamps(timestamps_path)
        self.video_informations = {}
        self.calculated_metrics = {}

        # the detection is completely wrong
        self.correct_detection_keys = ["True positive", "True negative"]
        self.wrong_detection_keys = ["Middle gap", "Early detection", "Late detection"]

        self.start_offset_keys = ["Early start", "Late start"]
        self.end_offset_keys = ["Early end", "Late end"]

        self.false_positive_keys = ["Late end", "Late detection", "Early detection", "Early start"]
        self.false_negative_keys = ["Middle gap", "Late start", "Early end"]

    def get_timestamped_video_names(self):
        """
        Get timestamped video names.

        :return: List of video names.
        """
        return list(self.timestamps.keys())

    @staticmethod
    def __time_to_seconds(time_string: str):
        """
        Convert hours, minutes, and seconds to total seconds.

        :param time_string: Time string in the format "HH:MM:SS".
        :return: Total seconds, or None if the format is invalid.
        """
        split = time_string.split(":")
        if len(split) != 3:
            return None
        hours, minutes, seconds = map(int, split)
        return hours * 3600 + minutes * 60 + seconds

    @staticmethod
    def __get_interval(interval, default_start, default_end):
        """
        Safely get start and end from an interval with defaults for missing values.

        :param interval: List containing the start and end of the interval.
        :param default_start: Default start value if interval start is missing.
        :param default_end: Default end value if interval end is missing.
        :return: Tuple containing the start and end of the interval.
        """
        if len(interval) == 2:
            start, end = interval
            good_start = start if start is not None else default_start
            good_end = end if end is not None else default_end
        elif len(interval) == 1:
            good_start = interval[0] if interval[0] is not None else default_start
            good_end = default_end
        else:
            good_start, good_end = default_start, default_end
        return good_start, good_end

    @staticmethod
    def __before_true_event(metric_for_event: dict, t_start, d_start, last_end, t_end, d_end):
        """
        Handle metrics before the true event.

        :param metric_for_event: Dictionary to store metrics for the event.
        :param t_start: Start time of the true event.
        :param d_start: Start time of the detected event.
        :param last_end: End time of the last detected event.
        :param t_end: End time of the true event.
        :param d_end: End time of the detected event.
        """
        # gaps - true negatives
        metric_for_event["True negative"] += max(0, min(t_start, d_start) - min(last_end, t_start))
        # detections - false positives
        mistake_before_start = max(0, min(t_start, d_end) - min(d_start, t_start))
        if d_end < t_start and mistake_before_start > 0:
            metric_for_event["Early detection"] += mistake_before_start
            metric_for_event["Early detection count"] += 1
        else:
            metric_for_event["Early start"] += mistake_before_start

    @staticmethod
    def __during_true_event(metric_for_event: dict, t_start, d_start, last_end, t_end, d_end):
        """
        Handle metrics during the true event.

        :param metric_for_event: Dictionary to store metrics for the event.
        :param t_start: Start time of the true event.
        :param d_start: Start time of the detected event.
        :param last_end: End time of the last detected event.
        :param t_end: End time of the true event.
        :param d_end: End time of the detected event.
        """
        # gaps - false negatives
        detection_gap_length = max(0, min(t_end, d_start) - max(t_start, last_end))
        if last_end > t_start and t_start < t_end and detection_gap_length > 0:
            metric_for_event["Middle gap"] += detection_gap_length
            metric_for_event["Middle gap count"] += 1
        elif last_end < t_start:
            metric_for_event["Late start"] += detection_gap_length
        elif d_end < t_end:
            metric_for_event["Early end"] += detection_gap_length

        # detections - true positives
        metric_for_event["True positive"] += max(0, min(t_end, d_end) - max(t_start, d_start))

    @staticmethod
    def __after_true_event(metric_for_event: dict, t_start, d_start, last_end, t_end, d_end):
        """
        Handle metrics after the true event.

        :param metric_for_event: Dictionary to store metrics for the event.
        :param t_start: Start time of the true event.
        :param d_start: Start time of the detected event.
        :param last_end: End time of the last detected event.
        :param t_end: End time of the true event.
        :param d_end: End time of the detected event.
        """
        # gaps - true negaives
        metric_for_event["True negative"] += max(0, max(t_end, d_start) - max(t_end, last_end))
        # detections - false positives
        late_detection_length = max(0, max(t_end, d_end) - max(t_end, d_start))
        if t_end < d_start and late_detection_length > 0:
            # started detecting after end of true event

            metric_for_event["Late detection"] += late_detection_length
            metric_for_event["Late detection count"] += 1
        else:
            # kept detecting after true event ended
            metric_for_event["Late end"] += late_detection_length

    def calculate_false_detections(self, video_name):
        """
        Calculate false detections for a given video.

        :param video_name: Name of the video to calculate false detections for.
        :return: Dictionary containing calculated metrics for the video.
        """
        self.calculated_metrics[video_name] = {}
        detected_events = self.video_informations[video_name]["statuses"]
        video_length = self.video_informations[video_name]["length"]
        # Process each event type
        for event_key, det_intervals in detected_events.items():
            true_interval = self.action_bounds[video_name][event_key]
            t_start, t_end = Metrics.__get_interval(true_interval, 0, video_length)
            self.calculated_metrics[video_name][event_key] = {"True positive": 0, "True negative": 0,
                                                              # Correct detections
                                                              "Early detection": 0, "Middle gap": 0,
                                                              "Late detection": 0,  # Wrong detection
                                                              "Early detection count": 0, "Middle gap count": 0,
                                                              "Late detection count": 0,  # Amount of wrong detections
                                                              "Early start": 0, "Late start": 0,  # Start offsets
                                                              "Early end": 0, "Late end": 0,  # End offsets
                                                              "Check": video_length}  # Check after the metric calculation has to be 0 - each frame has to be in some group above

            # Calculate metrics across detection intervals
            if true_interval is not None:
                last_end = 0
                for det_interval in det_intervals:
                    d_start, d_end = Metrics.__get_interval(det_interval, 0, video_length)
                    Metrics.__before_true_event(self.calculated_metrics[video_name][event_key],
                                                t_start, d_start, last_end, t_end, d_end)
                    Metrics.__during_true_event(self.calculated_metrics[video_name][event_key],
                                                t_start, d_start, last_end, t_end, d_end)
                    Metrics.__after_true_event(self.calculated_metrics[video_name][event_key],
                                               t_start, d_start, last_end, t_end, d_end)
                    # update last end
                    last_end = d_end

                # from last end until end of video
                # detection ended before
                self.calculated_metrics[video_name][event_key]["Early end"] += max(0, t_end - last_end)
                # last gap until the end
                self.calculated_metrics[video_name][event_key]["True negative"] += max(0, video_length - max(t_end,
                                                                                                             last_end))

            else:  # if there was no true detection - everythng is like an early detection
                for det_interval in det_intervals:
                    d_start, d_end = Metrics.__get_interval(det_interval, 0, video_length)
                    self.calculated_metrics[video_name][event_key]["Early detection"] += d_end - d_start
            # check correct
            for key, metric in self.calculated_metrics[video_name][event_key].items():
                if key != "Check" or not key.endswith("count"):
                    self.calculated_metrics[video_name][event_key]["Check"] -= metric
        return self.calculated_metrics[video_name]

    @staticmethod
    def __read_timestamps(timestamps_path: str):
        """
        Read timestamps from a file and convert them into seconds.

        :param timestamps_path: Path to the timestamps file.
        :return: Tuple containing the timestamps dictionary and action bounds dictionary.
        """
        timestamps_dataframe = pd.read_csv(timestamps_path, comment="#", index_col="file_name")
        timestamps = {}
        action_bounds = {}
        for file_name, row in timestamps_dataframe.iterrows():
            timestamps[file_name] = {}

            for action, time_str in row.items():
                if pd.notna(time_str):  # Check if the time string is not NaN
                    timestamps[file_name][action] = time_str
            action_bounds[file_name] = {
                "Parked": [Metrics.__time_to_seconds(timestamps[file_name]["parked"]),
                           Metrics.__time_to_seconds(timestamps[file_name]["left_parking"])],
                "Door(s) open": [Metrics.__time_to_seconds(timestamps[file_name]["doors_open"]),
                                 Metrics.__time_to_seconds(timestamps[file_name]["doors_closed"])],
                "Tunnel attached": [Metrics.__time_to_seconds(timestamps[file_name]["tunnel_attached"]),
                                    Metrics.__time_to_seconds(timestamps[file_name]["tunnel_detached"])]
            }
        return timestamps, action_bounds

    def calculate_metrics(self):
        """
        Calculate metrics for all videos in video_informations.
        """
        for video_name in self.video_informations:
            self.calculate_false_detections(video_name)

    def load_video_information(self, file_path: str):
        """
        Load video information from a JSON file.

        :param file_path: Path to the JSON file.
        """
        # Open the JSON file for reading
        with open(file_path, 'r') as file:
            # Load data from JSON file into a Python dictionary
            self.video_informations = json.load(file)

    def print_formated_metrics(self, metrics):
        for event_key, metric_mean in metrics.items():
            print(event_key)
            print(30 * "-")
            for metric_key, metric_result in metric_mean.items():
                if metric_result != 0 and metric_key not in self.correct_detection_keys:
                    print(metric_key, metric_result)
            print(30 * "_")

    def print_mean_video_metrics(self, video_names):
        """
        Print mean metrics for multiple videos.

        :param video_names: List of video names to print metrics for.
        """
        # Initialize metric_mean dictionary
        metric_means = {}
        for video_name in video_names:
            for event_key, metric_results in self.calculated_metrics[video_name].items():
                metric_means[event_key] = {}
                for metric_key in metric_results:
                    metric_means[event_key][metric_key] = 0

        # calculate mean results
        for video_name in video_names:
            for event_key, metric_results in self.calculated_metrics[video_name].items():
                for metric_key, result in metric_results.items():
                    metric_means[event_key][metric_key] += result / len(video_names)

        # print the results
        self.print_formated_metrics(metric_means)

    def print_worst_video_metrics(self, video_names):
        """
        Print worst metrics for multiple videos.

        :param video_names: List of video names to print metrics for.
        """
        # Initialize metric_mean dictionary
        worst_metrics = {}
        for video_name in video_names:
            for event_key, metric_results in self.calculated_metrics[video_name].items():
                worst_metrics[event_key] = {}
                for metric_key in metric_results:
                    worst_metrics[event_key][metric_key] = 0

        # calculate mean results
        for video_name in video_names:
            for event_key, metric_results in self.calculated_metrics[video_name].items():
                for metric_key, result in metric_results.items():
                    if result > worst_metrics[event_key][metric_key]:
                        worst_metrics[event_key][metric_key] = result

        # print the results
        self.print_formated_metrics(worst_metrics)

    def print_video_metrics(self, video_names):
        """
        Print metrics for individual videos.

        :param video_names: List of video names to print metrics for.
        """
        for video_name in video_names:
            self.calculate_false_detections(video_name)
            print(video_name + "\n")
            for event_key, metric_results in self.calculated_metrics[video_name].items():
                print(event_key)
                for metric_key, result in metric_results.items():
                    if result != 0 and metric_key not in self.correct_detection_keys:
                        print(metric_key, result)
                print(30 * "-")
            print(30 * "_")

    def plot_confusion_matrix(self, target_event_key=None):
        """
        Calculate overall confusion matrix values and plot the confusion matrix.

        :return: None
        """

        tp, fp, tn, fn = 0, 0, 0, 0
        for video_name, video_metric in self.calculated_metrics.items():
            for event_key, metric_result in video_metric.items():
                if target_event_key is None or event_key == target_event_key:
                    for metric_key, result in metric_result.items():
                        if metric_key == "True positive":
                            tp += result
                        elif metric_key == "True negative":
                            tn += result
                        elif metric_key in self.false_positive_keys:
                            fp += result
                        elif metric_key in self.false_negative_keys:
                            fn += result

        array = np.array([[tp, fn],
                          [fp, tn]])

        fig, ax = plt.subplots()
        cmaps = {None: plt.cm.Blues, "Door(s) open": plt.cm.Reds, "Tunnel attached": plt.cm.Purples,
                 "Parked": plt.cm.Greens}
        im = ax.imshow(array, interpolation='nearest', cmap=cmaps[target_event_key])
        ax.figure.colorbar(im, ax=ax)
        ax.set(xticks=np.arange(array.shape[1]),
               yticks=np.arange(array.shape[0]),
               xticklabels=['Yes', 'No'], yticklabels=['Yes', 'No'],
               title='Confusion Matrix' if target_event_key is None else target_event_key,
               ylabel='True label',
               xlabel='Predicted label')

        # Rotate the tick labels and set their alignment.
        plt.setp(ax.get_xticklabels(), rotation=45, ha="right", rotation_mode="anchor")

        # Loop over data dimensions and create text annotations.
        thresh = array.max() / 2.
        for i in range(array.shape[0]):
            for j in range(array.shape[1]):
                ax.text(j, i, int(array[i, j]),
                        ha="center", va="center", size=20,
                        color="white" if array[i, j] > thresh else "black")
        plt.show()
