import os
import cv2
from matplotlib import pyplot as plt
import numpy as np
import pandas as pd
from sklearn.cluster import DBSCAN


def read_wheel_occurrences(folder_path, wheel_id):
    """
    Stores bounding box information for analysis in a pandas dataframe
    :return: pd.DataFrame with wheel positions and sizes
    """
    wheel_occurrences = list()
    for filename in os.listdir(folder_path):
        if filename.endswith('.txt'):
            with open(os.path.join(folder_path, filename), 'r') as file:
                for line in file:
                    parts = line.split()  # Split the line into parts
                    # line must have 5 parts - class_id, x, y, width, height
                    if len(parts) == 5 and parts[0] == wheel_id:  # Check if the line starts with '3' - wheel class_id
                        # Convert the next four parts to float and store them
                        x, y, width, height = map(float, parts[1:5])
                        wheel_occurrences.append((x, y, width, height))
    wheel_occurrences = pd.DataFrame(wheel_occurrences, columns=['x', 'y', 'width', 'height'])
    return wheel_occurrences


def store_wheel_positions(detections, wheel_id):
    wheel_occurrences = list()
    for result in detections:
        wheel_indices = (result.boxes.cls == wheel_id).nonzero(as_tuple=False).flatten()
        for wheel_index in wheel_indices:
            wheel_xywhn = result.boxes.xywhn[wheel_index].flatten().cpu().numpy()
            wheel_occurrences.append(wheel_xywhn)
    return pd.DataFrame(wheel_occurrences, columns=['x', 'y', 'width', 'height'])


def dbscan_clustering(wheel_data, eps, min_samples):
    # read just position from wheel data (ignore width and height)
    wheel_positions = wheel_data[["x", "y"]]
    # Apply DBSCAN clustering
    dbscan = DBSCAN(eps=eps, min_samples=min_samples, algorithm='ball_tree', metric='haversine')
    clusters = dbscan.fit_predict(wheel_positions)
    return clusters

def get_cluster_centers(wheel_positions, clusters):
    unique_clusters = set(clusters)
    cluster_centers = list()
    for cluster in sorted(unique_clusters):
        if cluster != -1:
            # Select data points that belong to the current cluster (or noise)
            cluster_points = wheel_positions[clusters == cluster]
            average_position = np.mean(cluster_points, axis=0)
            cluster_centers.append(average_position)
    return pd.DataFrame(cluster_centers)

def plot_clusters(wheel_positions, image, clusters):
    unique_clusters = set(clusters)
    # Plotting the clusters
    # Create the plot
    fig, ax = plt.subplots()
    ax.imshow(cv2.cvtColor(image, cv2.COLOR_BGR2RGB), extent=[0, 1, 0, 1], aspect="auto")
    # Colors for the clusters (plus black for noise)
    colors = plt.cm.rainbow(np.linspace(0, 1, len(unique_clusters)))
    outlier_positions = wheel_positions[clusters == -1]
    ax.scatter(outlier_positions.x, 1 - outlier_positions.y, c="k", label="Outliers")
    for cluster_label, color in zip(unique_clusters, colors):
        # show everything but outliers
        if cluster_label != -1:
            cluster_label_name = f'Cluster {cluster_label}'
            # Select data points that belong to the current cluster (or noise)
            cluster_points = wheel_positions[clusters == cluster_label]

            average_position = np.mean(cluster_points, axis=0)
            print(f"{cluster_label_name} average position is {average_position}.")
            # Plot the points with the color and label
            ax.scatter(cluster_points['x'], 1 - cluster_points['y'], c=[color], label=cluster_label_name)
    plt.title('DBSCAN Clustering')
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.legend()
    plt.show()