\chapter{Testování}
\begin{chapterabstract}
V předchozí kapitole jsem popsal postup vytváření řešení detekce třech klíčových činností na letišti. Na validační množině jsem testoval jeho chování a podle toho přidával další videa do trénovací množiny pro model, ladil jsem timeout a konfidenční limity pro jednotlivé třídy.
V této kapitole co nejlépe otestuji fungování vyladěného řešení na testovacích datech a zhodnotím použitelnost jeho výsledků. Použiji k tomu metriky vydefinované v sekci \ref{sec:metrics}. Na testování používám dvanáct videí. U těchto videí využiji postup popsaný v sekci \ref{sec:metric_implementation}. 
\end{chapterabstract}

\section{Průběh testování}
Na testování jsem vybral dvanáct videí, tři pro každou kategorii světelných podmínek. Tato videa jsem prohlédl a označil jsem reálné intervaly, ve kterých dochází ke klíčovým činnostem. Nechal jsem naše řešení, aby prošlo každé video a také označilo intervaly, kdy ke klíčovým činnostem docházelo. Následně jsem pro každé video vypočítal odchylky a chyby, kterých se řešení dopustilo. Průměrné hodnoty metrik můžeme vidět v tabulce \ref{tab:testing_metric}. Průměrné zpoždění začátku a konce detekcí u otevření dveří a připojeného tunelu je velmi podobné hodnotám timeoutu, který je pro tyto třídy nastaven. Takové zpoždění je tedy očekávané a značí, že naše řešení ve většině případů funguje přesně tak jak má.

Důležité ovšem je i vědět, jak se naše řešení chová v méně příznivých podmínkách. V tabulce \ref{tab:testing_metric_worst} můžeme tedy vidět nejhorší chyby a odchylky, kterých se naše řešení daných dvanácti videích dopustilo. 

Timeouty pro potvrzování detekcí jsem nastavil na \textbf{12 s} pro dveře, \textbf{10 s} pro tunel a \textbf{3 s} pro parkování. O ladění jsem psal v sekci \ref{sec:timeout_tuning}. Při testování jsem nijak neměnil konfiguraci řešení. 

Na obrázku \ref{fig:confusion_matrix_all} můžeme vidět matici záměn spojenou pro všechny klíčové činnosti. Jedná se o součet všech hodnot všech tří klíčových činností. Čísla na hlavní diagonále představují počet snímků, ve kterých došlo ke správnému určení situace. Vedlejší diagonála počítá snímky, kdy došlo k záměně. K detekci docházelo jednou za sekundu, takže jeden snímek odpovídá jedné sekundě záznamu.

\newpage
\section{Hodnocení výsledků jednotlivých klíčových činností}
\textit{V této sekci zhodnotím výsledky chování detekcí každé klíčové činnosti. Proberu jejich chyby a odchylky a okomentuji, proč k nim dochází.}
\subsection{Otevírání dveří}
Nejhorší hodnotou v metrikách je o čtyřicet jedna sekund opožděná detekce stavu otevření dveří. Toto zpoždění bylo způsobeno faktem, že náš model začal dveře s jistotou brát jako otevřené, až po jejich úplném otevření -- obrázek \ref{fig:door_open_detection}. Při otevírání se střídaly stavy bez detekce -- obrázek \ref{fig:door_low_confidence}, se stavy, kdy model otevřené dveře sice detekoval, ale s nízkou konfidencí -- obrázek \ref{fig:door_no_detection}.

Třináct sekund dlouhá mezera v detekci měla podobnou příčinu, jako předchozí případ, ovšem problém nastal při zavírání dveří. Po celou dobu byly dveře otevřené a model to zaznamenával bez problému. Při zavírání na dobu delší než timeout přestal otevření dveří detekovat. Na posledních třináct sekund detekci obnovil a tím vznikla mezera. Následně se dveře zavřely, model to správně zaznamenal a detekci ukončil.

Obě dvě výše zmíněné situace nastaly na videích pořízených v noci zaznamenávající větší letadla. Detekce zaparkování dveří je u těchto případů spolehlivá po dobu, kdy jsou plně otevřené. Při otevírání a zavírání v nočních podmínkách je nutné počítat s menší spolehlivostí. Proces otevírání a zavírání dveří není delší než minuta. U menších letadel se jedná o pár sekund, proto s nimi nejsou problémy. Takové odchylky jsou pro letiště přípustné, vzhledem k tomu, že všechny časy se zaznamenávají v minutách.

Na obrázku \ref{fig:confusion_matrix_doors} můžeme vidět matici záměn pro detekci otevření dveří. Ve zhruba 12 hodinách záznamu 117 sekund řešení mylně prohlašovalo, že jsou dveře otevřené, i když byly v realitě zavřené a 179 sekund byl závěr, že jsou zavřené, ale byly otevřené. Model se za dvanáct hodin záznamu mýlil tedy pouhých pět minut.


\begin{figure}[H]
    \centering
    \begin{subfigure}{0.31\textwidth}
        \centering
        \includegraphics[width=\linewidth]{images/snapshots/door_low_confidence.png}
        \caption{nízká konfidence}
        \label{fig:door_low_confidence}
    \end{subfigure}
    \begin{subfigure}{0.29\textwidth}
        \centering
        \includegraphics[width=\linewidth]{images/snapshots/door_no_detection.png}
        \caption{bez detekce}
        \label{fig:door_no_detection}
    \end{subfigure}
    \begin{subfigure}{0.33\textwidth}
        \centering
        \includegraphics[width=\linewidth]{images/snapshots/door_open_detection.png}
        \caption{plně otevřené -- vysoká konfidence}
        \label{fig:door_open_detection}
    \end{subfigure}
    \caption{Problémy s detekcí dveří při otevírání/zavírání, zdroj: vlastní screenshoty}
    \label{fig:detection_issues}
\end{figure}

\subsection{Připojení tunelu}
Detekce připojení tunelu je časově přesnější. Je to především díky tomu, že proces připojování tunelu je výrazně rychlejší. Z toho vyplývá, že doba po kterou si model není jistý je výrazně kratší. Samotné sklopení stříšky trvá kolem pěti vteřin. 

Tunel za připojený považujeme až při plném připojení, na rozdíl od dveří. Ty považujeme za otevřené od počátku otevírání, až do plného zavření. Pro model je jednodušší detekovat toto úplné připojení, protože v daném stavu je výrazně déle, než v přechodném stavu připojování, a tak má model k disposici více trénovacích snímků.

Na obrázku \ref{fig:confusion_matrix_tunnel} můžeme vidět matici záměn pro připojení tunelu. Za 12 hodin analýza celkem 112 sekund přehlížela připojenost tunelu a 121 sekund naopak detekovala, že je připojen i když nebyl. Tato čísla přibližně odpovídají deseti sekundové délce timeoutu vynásobené dvanácti videi. Detekce modelu na testovacích datech byly dostatečně přesné, že by takto vysoký timeout nebyl potřeba. Na validačních datech jsem ho nastavoval spíše preventivně, aby nedocházelo k halucinacím událostí z důvodu pár sekundové chybné detekce. Do budoucna by se ale nejspíše mohl snižovat. 

\subsection{Parkování}
Nejlepší výsledky má detekce parkování. Nejhorší odchylka detekce začátku parkování byla tři sekundy. Detekce kola ve všech videích funguje bez problému, časové odchylky jsou zapříčiněné velikostí parkovacího místa. Jakmile se kolo letadla objeví v těchto mezích, spustí se timeout. Pokud se letadlo pohybuje pomalu, tak mu pár sekund trvá, než na daném místě zastaví. Rychlost příjezdu letadla tedy ovlivňuje tuto odchylku. 

Odchylka odjezdu je ve všech případech opožděná. Kolo letadla musí totiž nejprve opustit parkovací místo a až poté se spustí timeout. Pokud je kolo letadla po dobu tří sekund detekováno mimo parkovací místo, analyzér prohlásí, že letadlo opustilo parkovací místo.

Ve všech videích se detekce parkování chová podle očekávání. Časová odchylka příjezdu je $\pm 3$ s. Detekce odjezdu je v průměru opožděná o 6,5 s. Nejhorší zpoždění je 9 sekund. Tento výsledek je ideální pro použití v praxi.

Na obrázku \ref{fig:confusion_matrix_parked} můžeme vidět matici záměn pro zaparkování. Z této matice můžeme vyčíst, že model za celých 12 hodin jen 3 sekundy mylně prohlašoval, že letadlo není zaparkované, i když bylo. 99 sekund pak naopak hlásil, že letadlo parkuje, i když to nebyla pravda.




\begin{table}[H]
\caption{Průměr metrik finálního řešení na testovacích videích.}
\begin{tabular}{@{}llll@{}}
\toprule
Metrika               & Dveře otevřené & Zaparkováno & Tunel připojen  \\ \midrule
Brzký začátek         & -              & 1.66        & -               \\
Pozdní začátek        & 12             & 0.25        & 9.33            \\ [0.5ex]
Brzký konec           & 1.75           & -           & -               \\
Pozdní konec          & 9.75           & 6.58        & 10.08           \\ \midrule
Mezera v detekci      & 1.083          & -           & -               \\
Počet mezer v detekci & 0.083          & -           & -               \\ [0.5ex]
\bottomrule
\end{tabular}
\label{tab:testing_metric}
\end{table}



\begin{table}[H]
\caption{Nejhorší hodnoty metrik finálního řešení na testovacích videích.}
\begin{tabular}{@{}llll@{}}
\toprule
Metrika               & Dveře otevřené & Zaparkováno & Tunel připojen  \\ \midrule
Brzký začátek         & -              & 3           & -               \\
Pozdní začátek        & 41             & 3           & 21              \\ [0.5ex]
Brzký konec           & 21             & -           & -               \\
Pozdní konec          & 21             & 9           & 18              \\ \midrule
Mezera v detekci      & 13             & -           & -               \\
Počet mezer v detekci & 1              & -           & -               \\ [0.5ex]
\bottomrule
\end{tabular}
\label{tab:testing_metric_worst}
\end{table}

\begin{figure}[H]
    \centering
    
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=\linewidth]{images/snapshots/confusion_matrix_doors.png}
        \caption{otevření dveří}
        \label{fig:confusion_matrix_doors}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=\linewidth]{images/snapshots/confusion_matrix_tunnel.png}
        \caption{připojení tunelu}
        \label{fig:confusion_matrix_tunnel}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=\linewidth]{images/snapshots/confusion_matrix_parked.png}
        \caption{parkování}
        \label{fig:confusion_matrix_parked}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=\linewidth]{images/snapshots/confusion_matrix_all.png}
        \caption{součet}
        \label{fig:confusion_matrix_all}
    \end{subfigure}
    
    \caption{Matice záměn pro jednotlivé klíčové činnosti, zdroj: vlastní grafy}
\end{figure}